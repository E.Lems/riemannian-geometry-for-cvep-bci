# Riemannian geometry for cVEP BCI

Here I provide the implementations of LDA and MDM with two dimensionality reduction methods (cherry picking and CCA) for cVEP data. The data used is provided by the Donders institute and access should be granted.
Data should be obtained, with permission, from the Donders Data Repository https://data.donders.ru.nl/ at https://doi.org/10.34973/9txv-z787.
In Package versions.txt you will find all versions of everything that is used here and more.

## Different notebooks
- MDM.ipynb for MDM-CCA and MDM-cherry classification
- LDA.ipynb for LDA-CCA and LDA-cherry classification
- Statistical analysis.ipynb
- Visualizating with barplots.ipynb
- Learning curves and computation times.ipynb

## Pre-processing in Matlab
General things about the data:
- There are 8 channels: {'Fpz', 'T7', 'O1', 'POz', 'Oz', 'Iz', 'O2', 'T8'}
- 30 subjects
- 20 different symbols
- 100 trials
- Trial time is 31.5 seconds
- sampling frequency is 120 


A few pre-processing steps have already been done with the help of the jt_read_offline_data_gdf.m (which can be found when asking access from the Donders Repository). For this script to work you need a few toolboxes:
1. FieldTrip from the Donders Institute
2. Signal Processing Toolbox from MathWorks

Since I already went through the trouble of doing this I will shortly summarize the pre-processing that has been done and point you to the README.rtf as send to you.
1. Low-pass filter of 30 Hz applied
2. High-pass filter of 2 Hz applied
3. Downsampled
